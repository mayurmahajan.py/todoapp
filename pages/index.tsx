import React from 'react';
import { InitialToDoAppState } from '../components/Todo/state';
import { default as TodoComponent} from '../components/Todo/todo';

const Todo: React.FC = () => (
	<React.Fragment>
		<TodoComponent {...InitialToDoAppState}/>
	</React.Fragment>
);

export default Todo;
