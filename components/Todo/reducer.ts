
import { Draft } from 'immer';
import { ToDoActions, ToDoActionTypes } from './action';
import { ToDoAppState } from './state';

export function todoReducer(draft: Draft<ToDoAppState>, action: ToDoActions): Draft<ToDoAppState> {
	switch (action.type) {
	case ToDoActionTypes.GET_TODO_LIST_REQUEST: {
		draft.todoList.isLoading = true;
		break;
	}
	case ToDoActionTypes.GET_TODO_LIST_FAILURE: {
		draft.todoList.error = action.error;
		break;
	}
	case ToDoActionTypes.GET_TODO_LIST_SUCCESS: {
		draft.todoList.todos = action.data;
		break;
	}
	case ToDoActionTypes.UPDATE_TODO_ITEM_REQUEST: {
		draft.updateState = action.data;
		break;
	}
	case ToDoActionTypes.UPDATE_TODO_ITEM_SUCCESS: {
		draft.updateState = action.data;
		break;
	}
	case ToDoActionTypes.UPDATE_TODO_ITEM_FAILURE: {
		draft.updateState = action.data;
		break;
	}
	default:
		break;
	}
	return draft;
}