import { Todo } from '../../model/todo';
import { UpdateState } from './state';

export enum ToDoActionTypes {
  GET_TODO_LIST_REQUEST = 'GET_TODO_LIST_REQUEST',
  GET_TODO_LIST_SUCCESS = 'GET_TODO_LIST_SUCCESS',
  GET_TODO_LIST_FAILURE = 'GET_TODO_LIST_FAILURE',
  UPDATE_TODO_ITEM_REQUEST = 'UPDATE_TODO_ITEM_REQUEST',
  UPDATE_TODO_ITEM_SUCCESS = 'UPDATE_TODO_ITEM_SUCCESS',
  UPDATE_TODO_ITEM_FAILURE = 'UPDATE_TODO_ITEM_FAILURE'
}

export type ToDoActions =
  { type: typeof ToDoActionTypes.GET_TODO_LIST_REQUEST, isLoading: boolean } | 
  { type: typeof ToDoActionTypes.GET_TODO_LIST_SUCCESS, isLoading: boolean, data: Todo[] } |
  { type: typeof ToDoActionTypes.GET_TODO_LIST_FAILURE, isLoading: boolean, error: string } | 
  { type: typeof ToDoActionTypes.UPDATE_TODO_ITEM_REQUEST, data: UpdateState } | 
  { type: typeof ToDoActionTypes.UPDATE_TODO_ITEM_SUCCESS, data: UpdateState } | 
  { type: typeof ToDoActionTypes.UPDATE_TODO_ITEM_FAILURE, data: UpdateState, error: string };