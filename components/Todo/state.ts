import { Todo } from '../../model/todo';

export type ToDoState = {
	todos: Todo[],
	isLoading: boolean,
	error: string,
};

export enum UpdateState { Updating='Updating', Updated='Updated', UpdateFailed='UpdateFailed' }

export type ToDoAppState = {
	todoList: ToDoState,
	updateState: UpdateState
};

export const InitialToDoState: ToDoState = {
	todos: [],
	isLoading: false,
	error: '',
};

export const InitialToDoAppState: ToDoAppState = {
	todoList: InitialToDoState,
	updateState: UpdateState.Updated
};