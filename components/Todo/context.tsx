import moment from 'moment';
import React, { useContext, useEffect } from 'react';
import { useImmerReducer } from 'use-immer';
import { Todo } from '../../model/todo';
import { ToDoActionTypes } from './action';
import { todoReducer } from './reducer';
import { ToDoAppState, UpdateState } from './state';

const ToDoContext = React.createContext({});

interface IToDoHandler {
	getTodoList();
	handleToggleTodoCompleted(toDoId: string, newIsCompleted: boolean);
}

interface IToDoContext extends IToDoHandler, ToDoAppState { }

export const useToDoProvider = (): IToDoContext => {
	return useContext(ToDoContext) as IToDoContext;
};

type Props = { children: JSX.Element, initialState: ToDoAppState };

export const ToDoProvider: React.FC<Props> = (props: Props) => {
	const [state, dispatch] = useImmerReducer(todoReducer, props.initialState);

	const appState = state as ToDoAppState;
	const handlers = {} as IToDoContext;

	const mapToToDo = function (data): Todo[] {
		let todos = [] as Todo[];

		data.forEach(element => {
			const todo = new Todo();
			todo.id = element.id;
			todo.dueDate = element.dueDate === null ? null : moment(element.dueDate);
			todo.description = element.description;
			todo.isComplete = element.isComplete;

			todos.push(todo);
		});

		todos = todos.sort((todoItem1, todoItem2) => {				
			if(todoItem1.isComplete === true && todoItem2.isComplete === true) {
				return todoItem1.dueDate < todoItem2.dueDate ? 1 : -1;
			} else if(todoItem1.isComplete === true && todoItem2.isComplete === false) {
				return 1;
			} else if(todoItem1.isComplete === false && todoItem2.isComplete === false) {
				return todoItem1.dueDate < todoItem2.dueDate ? 1 : -1;
			} else if(todoItem1.isComplete === false && todoItem2.isComplete === true) {
				return -1;
			}
		});

		return todos;
	};

	handlers.getTodoList = async function () {
		dispatch({ type: ToDoActionTypes.GET_TODO_LIST_REQUEST, isLoading: true });
		try {
			const res = await fetch('https://944ba3c5-94c3-4369-a9e6-a509d65912e2.mock.pstmn.io/get', { headers: { 'X-Api-Key': 'PMAK-5ef63db179d23c004de50751-10300736bc550d2a891dc4355aab8d7a5c' } });
			const data = await res.json();
			const todoData = mapToToDo(data);
			dispatch({ type: ToDoActionTypes.GET_TODO_LIST_SUCCESS, isLoading: false, data: todoData });
		} catch (e) {
			dispatch({ type: ToDoActionTypes.GET_TODO_LIST_FAILURE, isLoading: false, error: e });
		}
	};

	useEffect(() => {
		if(appState.updateState===UpdateState.Updated || appState.updateState===UpdateState.UpdateFailed) {
			handlers.getTodoList();
		}
	}, [appState.updateState]);

	handlers.handleToggleTodoCompleted = async function (toDoId: string, newIsCompleted: boolean) {
		dispatch({ type: ToDoActionTypes.UPDATE_TODO_ITEM_REQUEST, data: UpdateState.Updating });
		try {
			await fetch(`https://944ba3c5-94c3-4369-a9e6-a509d65912e2.mock.pstmn.io/patch/${toDoId}`, { method: 'PATCH', headers: { 'X-Api-Key': 'PMAK-5ef63db179d23c004de50751-10300736bc550d2a891dc4355aab8d7a5c', 'Content-Type': 'application/json' }, body: JSON.stringify({'isComplete': newIsCompleted}) });
			dispatch({ type: ToDoActionTypes.UPDATE_TODO_ITEM_SUCCESS, data: UpdateState.Updated });
		} catch (e) {
			dispatch({ type: ToDoActionTypes.UPDATE_TODO_ITEM_FAILURE, data: UpdateState.UpdateFailed, error: e });
		}
	};

	return (
		<ToDoContext.Provider value={{ ...state, ...handlers }}>
			{props.children}
		</ToDoContext.Provider>
	);
};