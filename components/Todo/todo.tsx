import React from 'react';
import { Todo as TodoModel } from '../../model/todo';
import { ToDoProvider, useToDoProvider } from './context';
import { ToDoAppState } from './state';
import styled from 'styled-components';
import moment from 'moment';
import Head from 'next/head'

const ToDoConsumerContainer = styled.div`
  display: flex;
  align-content: center;
  justify-content: center;
`;

const Todo: React.FC<ToDoAppState> = (todos: ToDoAppState) => {
	return (
		<React.Fragment>
			<Head>
				<title>TODO App</title>
				<link rel="icon" href="/favicon.ico" />
			</Head>
			<ToDoProvider initialState={todos}>
				<ToDoConsumerContainer>
					<TodoConsumer />
				</ToDoConsumerContainer>
			</ToDoProvider>
		</React.Fragment>
	);
};

const ToDoContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-content: center;
  justify-content: center;
`;

const ToDoHeader = styled.div`
  display: flex;
  background-color: #DDD5DB;
  max-width: 700px;
  max-height: 100px;
`;

const ToDoItemList = styled.div`
  display: flex;
  flex-direction: column;
  align-content: center;
  justify-content: center;
`;

const ToDoItem = styled.div`
  border: 1px solid #eee;
  margin: 2px 0;
  display: flex;
  justify-content: space-between;
  padding: 1px;
  width: 400px;
  background-color: #DDD5DB;
`;

const CompletedToDoItem = styled.div`
  border: 1px solid #eee;
  margin: 2px 0;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 1px;
  width: 400px;
  background-color: #71A46D;
`;

const OverDueToDoItem = styled.div`
  border: 1px solid #eee;
  margin: 2px 0;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 1px;
  width: 400px;
  background-color: #FF9633;
`;

const StrikedDiv = styled.div`
  text-decoration: line-through
`;

const DueDateContainer = styled.div`
  padding: 1px 1px;
  border: 1px solid black;
`;

const DueDateEmptyContainer = styled.div`
  padding: 28px 28px;
`;

const TodoConsumer: React.FC = () => {
	const context = useToDoProvider();
	return (
		<ToDoContainer>
		<ToDoHeader>Todos</ToDoHeader>
		<ToDoItemList>
			{context.todoList.todos && context.todoList.todos.map((item: TodoModel) => {
				return (<TodoItem key={item.id} {...item} />);
			})}
		</ToDoItemList>
		</ToDoContainer>
	);
};

const TodoItem: React.FC<TodoModel> = (props: TodoModel) => {
	const context = useToDoProvider();
	const dt = props.dueDate ? props.dueDate.format("MM/DD/YYYY").toString() : null;

	const item = (<React.Fragment><div>
		<input type='checkbox'
			checked={props.isComplete}
			onChange={() => { context.handleToggleTodoCompleted(props.id, !props.isComplete); }} /></div>
		{props.isComplete === true ? <StrikedDiv>{props.description}</StrikedDiv> : <p>{props.description}</p>}
		{dt ? <DueDateContainer>{dt}</DueDateContainer> : <DueDateEmptyContainer />}</React.Fragment>)

	if (props.dueDate && props.dueDate.isAfter(moment.now())) return <OverDueToDoItem>{item}</OverDueToDoItem>;
	if (props.isComplete === true) return <CompletedToDoItem>{item}</CompletedToDoItem>;
	return <ToDoItem>{item}</ToDoItem>;
};

export default Todo;