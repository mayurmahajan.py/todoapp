### About
See your todo items and update it.

### How to start?
```bash
npm install
npm run dev
npm run build
npm run start
```