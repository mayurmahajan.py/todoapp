import { Moment } from 'moment';

export class Todo {
    id: string;
    description: string;
    isComplete: boolean;
    dueDate: Moment;
}
